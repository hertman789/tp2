package br.com.cotemig.fappfilas_a;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterLoja extends BaseAdapter {

    private List<Loja> lojas;
    private Activity activity;

    public AdapterLoja(Activity activity, List<Loja> lojas) {
        this.activity = activity;
        this.lojas = lojas;
    }

    @Override
    public int getCount() {
        return lojas.size();
    }

    @Override
    public Object getItem(int i) {
        return lojas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lojas.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View viewAdapter = activity.getLayoutInflater().inflate(R.layout.layout_lista_activity, viewGroup, false);

       // TextView idLoja = viewAdapter.findViewById(R.id.textIdLoja); //--NÃO ESTOU CONSEGUINDO RETORNAR O ID NO LISTVIEW
        TextView nome = viewAdapter.findViewById(R.id.textNomeL);
        TextView cnpj = viewAdapter.findViewById(R.id.textCnpj);
        TextView telefone = viewAdapter.findViewById(R.id.textTelefoneLoja);
        TextView email = viewAdapter.findViewById(R.id.textEmailLoja);

        Loja l = lojas.get(i);
       // idLoja.setText(l.getId());
        nome.setText(l.getNomeLoja());
        cnpj.setText(l.getCnpj());
        telefone.setText(l.getTelefoneLoja());
        email.setText(l.getEmailLoja());

        return viewAdapter;
    }
}
