package br.com.cotemig.fappfilas_a;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

//classe onde os cadastros serão realizados
public class CadastroLoja extends AppCompatActivity {

    private EditText nomeLoja;
    private EditText cnpj;
    private EditText emailLoja;
    private EditText telefoneLoja;
    private EditText senha;
    private LojaDAO lojaDAO;
    private Loja loja = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_vendas_activity); // chama o layout de cadastro, coloquei aqui para testar

        nomeLoja        = findViewById(R.id.editNomeLoja);
        cnpj            = findViewById(R.id.editCnpj);
        emailLoja       = findViewById(R.id.editEmailL);
        telefoneLoja    = findViewById(R.id.editTelefoneL);
        senha           = findViewById(R.id.editTextSenha);
        lojaDAO = new LojaDAO(this);

        Intent it = getIntent();
        if (it.hasExtra("loja")){
            loja = (Loja) it.getSerializableExtra("loja");
            nomeLoja.setText(loja.getNomeLoja());
            cnpj.setText(loja.getCnpj());
            emailLoja.setText(loja.getEmailLoja());
            telefoneLoja.setText(loja.getTelefoneLoja());
            senha.setText(loja.getSenha());
        }
    }

    public void cadastraLoja(View view){

        if(loja == null){
            loja = new Loja();
            loja.setNomeLoja(nomeLoja.getText().toString());
            loja.setCnpj(cnpj.getText().toString());
            loja.setEmailLoja(emailLoja.getText().toString());
            loja.setTelefoneLoja(telefoneLoja.getText().toString());
            loja.setSenha(senha.getText().toString());

            long id = lojaDAO.inserirLoja(loja);
            Toast.makeText(this, "Cadastro "+ id +" realizado com sucesso", Toast.LENGTH_LONG).show(); //MSG DE SUCESSO
        } else {
            loja.setNomeLoja(nomeLoja.getText().toString());
            loja.setCnpj(cnpj.getText().toString());
            loja.setEmailLoja(emailLoja.getText().toString());
            loja.setTelefoneLoja(telefoneLoja.getText().toString());
            loja.setSenha(senha.getText().toString());

            lojaDAO.atualizaLoja(loja);
            Toast.makeText(this, "Dados atualizados com sucesso", Toast.LENGTH_LONG).show();
        }
    }
}