package br.com.cotemig.fappfilas_a;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ConexaoDB extends SQLiteOpenHelper {
    private static final String name = "banco.db";
    private static final int version = 1;


    public ConexaoDB(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE loja (id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "nomeloja varchar(50), cnpj varchar(50), emailloja varchar(50), telefoneloja varchar(50), senha varchar(50))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
