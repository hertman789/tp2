package br.com.cotemig.fappfilas_a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class LojaDAO {

    private ConexaoDB conexaoDB;
    private SQLiteDatabase banco;

    public LojaDAO(Context context){
        conexaoDB = new ConexaoDB(context);
        banco = conexaoDB.getWritableDatabase();

    }

    public long inserirLoja(Loja loja){
        ContentValues values = new ContentValues();
        values.put("nomeloja", loja.getNomeLoja());
        values.put("cnpj", loja.getCnpj());
        values.put("emailloja", loja.getEmailLoja());
        values.put("telefoneloja", loja.getTelefoneLoja());
        values.put("senha", loja.getSenha());

        return banco.insert("loja", null, values);

    }

    public List<Loja> carregaLojas(){

        List<Loja> loja = new ArrayList<>();
            Cursor cursor = banco.query("loja", new String[]{"id", "nomeloja", "cnpj", "emailloja", "telefoneloja", "senha"}, null, null, null, null, null);
        while(cursor.moveToNext()){
            Loja l = new Loja();
            l.setId(cursor.getInt(0));
            l.setNomeLoja(cursor.getString(1));
            l.setCnpj(cursor.getString(2));
            l.setEmailLoja(cursor.getString(3));
            l.setTelefoneLoja(cursor.getString(4));
            l.setSenha(cursor.getString(5));
            loja.add(l);
        }
        return loja;
    }

    public void excluirLoja(Loja l){
        banco.delete("loja", "id = ?", new String[]{l.getId().toString()});

    }

    public void atualizaLoja(Loja l){
        ContentValues values = new ContentValues();
        values.put("nomeloja", l.getNomeLoja());
        values.put("cnpj", l.getCnpj());
        values.put("emailloja", l.getEmailLoja());
        values.put("telefoneloja", l.getTelefoneLoja());
        values.put("senha", l.getSenha());
        banco.update("loja", values,"id = ?", new String[]{l.getId().toString()});
    }

}
