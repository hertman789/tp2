package br.com.cotemig.fappfilas_a;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

//ESTOU NA DÚVIDA A IMPLEMENTAÇÃO SERIA ASSIM,
// PQ PENSO QUE UM CADASTRO PODE TER MAIS DE UMA LOJA,
// ENTÃO FAZ SENTIDO RETORNAR UMA LISTA
public class lista_loja_activity extends AppCompatActivity {

    private ListView listViewLoja;
    private LojaDAO lojaDAO;
    private List<Loja> loja;
    private List<Loja> lojaListada = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_loja_activity);

        listViewLoja = findViewById(R.id.listaLoja);
        lojaDAO = new LojaDAO(this);
        loja = lojaDAO.carregaLojas();
        lojaListada.addAll(loja);

        //ADAPTADOR P LISTAR
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lojaListada);
        //AdapterLoja adapter = new AdapterLoja(this, lojaListada); --- NÃO ESTOU CONSEGUINDO EXIBIR AS INFORMAÇÕES DO ADAPTER P TRAZER O LAYOUT DA LISTA
        listViewLoja.setAdapter(adapter);

        registerForContextMenu(listViewLoja);

    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cadastro_consulta, menu);

        SearchView sv = (SearchView) menu.findItem(R.id.menuBuscar).getActionView();
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                procuraLoja(s);
                System.out.println("Digitou" + s);
                return false;
            }
        });
        return true;
    }

    //AO PRESSIONAR O ITEM DA LISTA UM MENU SERÁ ABERTO
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater i = getMenuInflater();
        i.inflate(R.menu.alterar_excluir, menu);
    }


    public void procuraLoja(String nome){
        lojaListada.clear();
        for (Loja l : loja){
            if (l.getNomeLoja().toLowerCase().contains(nome.toLowerCase())){
                lojaListada.add(l);
            }
        }
        listViewLoja.invalidateViews();
    }

    public void excluir(MenuItem item){
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        final Loja excluirLoja = lojaListada.get(menuInfo.position);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Atenção")
                .setMessage("Deseja excluir a loja?")
                .setNegativeButton("Não", null)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        lojaListada.remove(excluirLoja);
                        loja.remove(excluirLoja);
                        lojaDAO.excluirLoja(excluirLoja);
                        listViewLoja.invalidateViews();
                    }
                }).create();
        dialog.show();
    }

    public void adicionar(MenuItem item){
        Intent it = new Intent(this, CadastroLoja.class);
        startActivity(it);
    }

    public void atualizarLoja(MenuItem item){
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        final Loja atualizaLoja = lojaListada.get(menuInfo.position);

        Intent it = new Intent(this, CadastroLoja.class);
        it.putExtra("loja", atualizaLoja);
        startActivity(it);
    }

    @Override
    public void onResume(){
        super.onResume();
        loja = lojaDAO.carregaLojas();
        lojaListada.clear();
        lojaListada.addAll(loja);
        listViewLoja.invalidateViews();
    }



}